A2Z Vocab
==================================================
Contributors:<br/>
Asok Kalidass Kalisamy – asok@dal.ca<br/>
Kartik Puri – kr394429@dal.ca<br/>
Kundan Kumar – kundan@dal.ca<br/>
Shalav Verma – shalav@dal.ca<br/>
Yuvaraj Ganesan – yuvaraj.ganesan@dal.ca<br/>

-------------------------------------------------------------------------------

Code Directory Structure

code/
  - atozvocab : Contains the code for the android application.
  - data/firestore: Contains script to upload data to the Firestore database.

-------------------------------------------------------------------------------

How to run ?
  -  Load the Android project in atozvocab folder in Android studio.
  -  Sync to get all the dpendencies.
  -  Run to install the application.

-------------------------------------------------------------------------------

Uploading data to Firestore 
 - Requirements: Python 2.7, Virtualenv
 - Step 1: Create a virtual environment pointing to code/data/firestore.
   Refer http://pymote.readthedocs.io/en/latest/install/windows_virtualenv.html
 - Step 2: In created virtual environment, run `pip install -r requirements.txt` 
   to install required libs.
 - Step 3: Run `python upload.py` to upload content from csv to Firestore database.

**Note**: This step is not needed if the application uses the defualt database 
settings. This step is required only when a new database is to be created.

-------------------------------------------------------------------------------
Application Dependencies:
1.  Cloud Frestore : https://firebase.google.com/docs/firestore/
2.  Google Play Service: Version 11.7.x
