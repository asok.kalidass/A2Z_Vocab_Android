import csv
import json
import firebase_admin
from firebase_admin import credentials as fac
from firebase_admin import firestore as fs


class Upload:

	def __init__(self):
		cred = fac.Certificate('./atozvocab-b89c57146dd8.json')
		firebase_admin.initialize_app(cred)
		self.db = fs.client()
		self.wordsModel = "WordsModel"

	def get_typecasted_dict(self, ip):
		uip = {unicode(x):unicode(ip[x].decode('ascii', 'ignore').encode('ascii')) for x in ip.keys()}
		bool_keys = [u'gre', u'sat', u'general']
		for k in bool_keys:
			uip[k] = True if uip.get(k) == u'TRUE' else False
		uip[u'flag'] = u'Y'
		return uip

	def upload_from_csv(self, path):
		with open(path, 'r') as fh:
			reader = csv.DictReader(fh)
			for row in reader:
				word = row.get('word', None)
				if word:
					uRow = self.get_typecasted_dict(row)
					doc_ref = self.db.collection(self.wordsModel).document(word.lower().strip())
					ret = doc_ref.set(uRow)
					print str(uRow).encode('utf-8')
					#break
				else:
					print "Error >> Word Missing!",row

if __name__=="__main__":
	upload = Upload()
	upload.upload_from_csv("./data_v1.csv")





