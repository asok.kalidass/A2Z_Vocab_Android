package ca.dal.atozvocab.utils;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import ca.dal.atozvocab.R;
import ca.dal.atozvocab.models.WordsModel;

/**
 * Created by kundan on 2017-11-07.
 */

public class WordsAdapter extends RecyclerView.Adapter<WordsAdapter.WordsViewHolder> {

    public enum E_VIEW_TYPE{
        NONE,
        WORDS_LIST,
        MARKED_WORDS
        //FLASH_CARDS
    }

    private Context mContext;
    private List<WordsModel> mWords;
    private WordsAdapterCallback mAdapterCallback;
    private E_VIEW_TYPE mViewType;

    public static class WordsViewHolder extends RecyclerView.ViewHolder{
        public CardView mCardView;
        public TextView mTvWord;
        public TextView mTvWordMeaning;
        public TextView mTvSentence;
        public Button mBtnReadAloud;
        public Button mCardSaveUnsaveBtn;

        public WordsViewHolder(View v){
            super(v);
            mCardView = (CardView) v.findViewById(R.id.card_view);
            mCardSaveUnsaveBtn = (Button) v.findViewById(R.id.card_btn_save_unsave);
            mTvWord = (TextView) v.findViewById(R.id.card_tv_word);
            mTvWordMeaning = (TextView) v.findViewById(R.id.card_tv_meaning);
            mTvSentence = (TextView) v.findViewById(R.id.card_tv_sentence);
            mBtnReadAloud = (Button) v.findViewById(R.id.card_btn_read);
        }
    }



    public WordsAdapter(List<WordsModel> words, E_VIEW_TYPE viewType, WordsAdapterCallback callback){
        mWords = words;
        this.mAdapterCallback = callback;
        this.mViewType = viewType;
    }


    @Override
    public WordsAdapter.WordsViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card, parent, false);
        WordsViewHolder vh = new WordsViewHolder(v);
        this.mContext=parent.getContext();
        return vh;
    }

    @Override
    public void onBindViewHolder(WordsViewHolder holder, int pos) {
        holder.mTvWord.setText(mWords.get(pos).getWord());
        holder.mTvWordMeaning.setText(mWords.get(pos).getMeaning());
        holder.mTvSentence.setText(mWords.get(pos).getSentence());

        if (mViewType.equals(E_VIEW_TYPE.WORDS_LIST) || mViewType.equals(E_VIEW_TYPE.NONE)){
            holder.mCardSaveUnsaveBtn.setText("Save");
        }else if (mViewType.equals(E_VIEW_TYPE.MARKED_WORDS) ){
            holder.mCardSaveUnsaveBtn.setText("Unsave");
        }

        holder.mCardSaveUnsaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String word ="";
                if (mViewType.equals(E_VIEW_TYPE.WORDS_LIST)){
                    word = mWords.get(pos).getWord();
                    mWords.remove(pos);
                    notifyItemRemoved(pos);
                    notifyItemRangeChanged(pos,mWords.size());

                }else if (mViewType.equals(E_VIEW_TYPE.MARKED_WORDS)){
                    word = mWords.get(pos).getWord();
                    mWords.remove(pos);
                    notifyItemRemoved(pos);
                    notifyItemRangeChanged(pos,mWords.size());
                    mAdapterCallback.onMarkWordsCallback(word);
                }
                mAdapterCallback.onMarkWordsCallback(word);
            }
        });

        //Read Aloud
        holder.mBtnReadAloud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapterCallback.onReadAloudCallback(holder.mTvWord.getText().toString() + "," + holder.mTvWordMeaning.getText().toString() +
                    "," + holder.mTvSentence.getText().toString());
            }
        });


    }

    @Override
    public int getItemCount() {
        return mWords.size();
    }

    public interface WordsAdapterCallback {
        void onMarkWordsCallback(String wordRemoved);
        void onReadAloudCallback(String word);
    }

}
