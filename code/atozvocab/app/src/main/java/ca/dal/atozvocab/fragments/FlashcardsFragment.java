package ca.dal.atozvocab.fragments;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ca.dal.atozvocab.R;
import ca.dal.atozvocab.models.WordsModel;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FlashcardsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FlashcardsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

/**
 This Fragment is responsible for displaying Flash Card UI
 */
public class FlashcardsFragment extends Fragment implements SensorEventListener {
    //Constant variable to maintain the unknown word frequency in dictionary
    private static final String WORD_FREQ = "WordFreq";
    private static final String COLLECTION_TYPE = "WordsModel";
    private static final String DOC_FIELD = "flag";
    private static final String KNOWN_WORD = "Y";
    private static final String UNKNOWN_WORD = "N";

    //Private Members
    private int knownWordFreq;
    private boolean isUnknown;
    private FirebaseFirestore mFirestore;
    private TextView txtWord;
    private TextView txtMeaning;
    private TextView txtSentence;
    private TextView txtLearning;
    private Button btnknownword;
    private Button btnUnknowword;
    private OnFragmentInteractionListener mListener;
    private View rootView;
    private static WordsModel wordModel;
    private  ListenerRegistration listenerRegistration = null;
    private boolean isCallback;
    private static Context context;

    // The following are used for the shake detection
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private Vibrator vibrator;
    int backTiltCount, fwdTiltCount = 0;

    //Constructor
    public FlashcardsFragment() {
        mFirestore = FirebaseFirestore.getInstance();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param unknownWordFreq 1. Tracks the unknown frequency last displayed
     * @return A new instance of fragment FlashcardsFragment.
     */
    public static FlashcardsFragment newInstance(int unknownWordFreq, String emailId) {
        FlashcardsFragment fragment = new FlashcardsFragment();
        Bundle args = new Bundle();
        args.putInt(WORD_FREQ, unknownWordFreq);
        args.putString("emailId", emailId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // ShakeDetector initialization
        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);

        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_flashcards, container, false);
        return  rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        btnknownword = (Button) getActivity().findViewById(R.id.btnKnowWord);
        btnUnknowword = (Button) getActivity().findViewById(R.id.btnUnKnowWord);
        //Known word click listener
        btnknownword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isUnknown = false;
                //show the next word
                mListener.onKnownWordClick(rootView, wordModel, knownWordFreq > 5 ? knownWordFreq = 0 : knownWordFreq++);
            }
        });
        //Unknown word click listener
        btnUnknowword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isUnknown = true;
                //show the next word
                mListener.onUnKnownWordClick(rootView, wordModel, UNKNOWN_WORD, 0);
            }
        });
        refreshFlashCard(rootView, wordModel);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mFirestore = FirebaseFirestore.getInstance();
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // ignore
    }

    /**
     * Even fires on change in sensor value
     * @param event sensor event object
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        float[] values = event.values;
        long curTime = System.currentTimeMillis();
        long lastUpdate = 0;
        if (event.sensor.getType() == 3) {

        }
        //Read the changes in accelerometer value
        if (event.sensor.getType() == 1) {
            //Detect the backward tilt if x coordinate falls below 1.5
            if(values[1] < -1.5 && backTiltCount == 0) {
                //indicate the user for vibration for any tilt
                vibrator.vibrate(100);
                backTiltCount++;
                fwdTiltCount =0;
                knownWordFreq = 0;
                setWordFlag(rootView, wordModel, UNKNOWN_WORD);
            }
            //Detect the forward tilt if y coordinate in range 8.6 ~ 9.99 or Z coordinate is less than 1
            else if(((values[1] > 8.6 && values[1] < 10) || values[2] < 1) && (fwdTiltCount==0)) {
                //indicate the user for vibration for any tilt
                vibrator.vibrate(100);
                fwdTiltCount++;
                backTiltCount = 0;
                knownWordFreq = knownWordFreq > 5 ? knownWordFreq = 0 : ++knownWordFreq;
                refreshFlashCard(rootView, wordModel);
            }
            //ignore remaining values
            else {
                backTiltCount =0;
                fwdTiltCount =0;

            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        //registers the Session Manager Listener onResume
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onPause() {
        //unregisters the Sensor Manager onPause
        sensorManager.unregisterListener(this);
        super.onPause();
    }

    /**
     * On click event for known word
     * @param view flashcard view object
     * @param wordsModel word model
     */
    public void onNextClick(View view, WordsModel wordsModel) {
        knownWordFreq = getArguments().getInt(WORD_FREQ);
        refreshFlashCard(view, wordsModel);
    }

    /**
     * On click event for Unknown word
     * @param view flashcard view object
     * @param wordsModel word model
     * @param flag unknown word flag
     */
    public void onDontKnowClick(View view, WordsModel wordsModel, String flag) {
        knownWordFreq = getArguments().getInt(WORD_FREQ);
        setWordFlag(view, wordsModel, flag);
    }
    /**
     * Refreshes the UI accordingly with known or unknown word based on the known word frequency
     * @param view Fragment View object
     */
    private void refreshFlashCard(View view, WordsModel wordsModel) {
        Query query = null;

        CollectionReference wordmodel = mFirestore.collection(COLLECTION_TYPE);

        //Handle the unknown work and set the flag if its known or unknown
        switch (knownWordFreq) {
            case 4:
                if(!isCallback) {
                    //reset the counter
                    knownWordFreq = 0;
                    //show the unknown word
                    CollectionReference ref = mFirestore.collection("UnknownWordUserModel").document(getArguments().getString("emailId")).collection("UnknownWords");
                    query = ref.whereEqualTo("flag", UNKNOWN_WORD).limit(50);
                    isUnknown = true;
                }
                else {
                    query = wordmodel.whereEqualTo(DOC_FIELD, KNOWN_WORD).limit(50);
                    isUnknown = false;
                }
                break;
            case 5:
                if(!isUnknown) {
                    //mark the unknown word to either known or unknown
                    knownWordFreq = 0;
                    query = wordmodel.whereEqualTo("flag", KNOWN_WORD).limit(50);
                    setWordFlag(view, wordsModel, KNOWN_WORD);
                    deleteDocument(wordsModel);
                }
                break;
            default:
                //show the known word
                query = wordmodel.whereEqualTo(DOC_FIELD, KNOWN_WORD).limit(50);
                isUnknown = false;
        }
        getWordList(view, wordsModel, query);
    }

    /**
     * Delete the known document
     * @param wordsModel document to be deleted
     */
    private void deleteDocument(WordsModel wordsModel) {
        mFirestore.collection("UnknownWordUserModel").document(getArguments().getString("emailId")).collection("UnknownWords").document(wordsModel.getWord())
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("deleteDocument", "DocumentSnapshot successfully deleted!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("deleteDocument", "Error deleting document", e);
                    }
                });
    }

    /**
     * Gets the word list
     * @param view flashcard view state
     * @param wordsModel wordsmodel obj
     * @param query firebase query conditions
     */
    private void getWordList(View view, WordsModel wordsModel, Query query) {

        //Call back listener to fetch from firebase
        listenerRegistration = query.addSnapshotListener((snapshot, e) -> {
            List<WordsModel> wModels = new ArrayList<WordsModel>();
            if (e != null) {
                Log.w("refreshFlashCard", "Listen failed.", e);
                return;
            }
            //Gets the word list from the firebase
            if (wModels.size() > 0) { wModels.clear(); }
            for (DocumentSnapshot document : snapshot.getDocuments()) {
                WordsModel wm = document.toObject(WordsModel.class);
                wModels.add(wm);
            }
            if (snapshot.getDocuments().size() == 0) {
                //restart the counter if no word is found
                knownWordFreq = 0;
                isCallback = true;
                refreshFlashCard(view, wordsModel);
                return;
            }
            setCardView(view, wModels);
            listenerRegistration.remove();
            isCallback = false;
        });
    }

    /**
     * Populates the UI with required data
     * @param wModels List of word document
     * @param rootView Fragment view object
     */
    private void setCardView(View rootView, List<WordsModel> wModels) {
        Random random;
        random = new Random();
        if (rootView == null) { rootView = getView(); }
        //Randomly picks wither known or unknown word and binds it with the UI elements
        wordModel = wModels.get(random.nextInt(wModels.size()));
        txtWord = (TextView) rootView.findViewById(R.id.card_tv_word);
        txtMeaning = (TextView) rootView.findViewById(R.id.card_tv_meaning);
        txtSentence = (TextView) rootView.findViewById(R.id.card_tv_sentence);
        txtLearning = (TextView) rootView.findViewById(R.id.txtLearning);
        txtWord.setText(wordModel.getWord());
        if (isUnknown) { txtLearning.setBackgroundColor(Color.YELLOW); txtLearning.setText("LEARNING"); }
        else { txtLearning.setBackgroundColor(Color.WHITE); txtLearning.setText("");}
        txtMeaning.setText(wordModel.getMeaning());
        txtSentence.setText(wordModel.getSentence());
    }

    /**
     * Updates the flag of a word to known - 'Y' and Unknown N'
     * @param wordsModel word model
     */
    private void setWordFlag(View view, WordsModel wordsModel, String flag) {
        DocumentReference ref = mFirestore.collection("UnknownWordUserModel").document(getArguments().getString("emailId")).collection("UnknownWords").document(wordsModel.getWord());
        wordsModel.setFlag(flag);
        ref
                .set(wordsModel)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.w("SetWordAsUnknown", "Flag updated!");
                        if (flag == "N") { refreshFlashCard(view, wordsModel); }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("SetWordAsUnknown", "Error in updating flag", e);
                    }
                });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     *
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onKnownWordClick(View view, WordsModel wordsModel, int wordFreq);
        void onUnKnownWordClick(View view, WordsModel wordsModel, String flag, int wordFreq);
    }
}
