package ca.dal.atozvocab.models;

import java.util.List;

/**
 * Created by kundan on 2017-11-07.
 */

public class WordsModel {
    private String word;
    private String meaning;
    private String sentence;
    private boolean gre;
    private boolean sat;
    private boolean general;
    private String location;
    private String Flag;
    public WordsModel() {}

    public WordsModel(String word, String meaning, String sentence,
                      boolean isGre, boolean isSat, boolean isGeneral, String location){
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public void setFlag(String Flag) {
        this.Flag = Flag;
    }

    public boolean isGre() {
        return gre;
    }

    public void setGre(boolean gre) {
        this.gre = gre;
    }

    public boolean isSat() {
        return sat;
    }

    public void setSat(boolean sat) {
        this.sat = sat;
    }

    public boolean isGeneral() {
        return general;
    }

    public void setGeneral(boolean general) {
        this.general = general;
    }

    public String getLocation() {
        return location;
    }

    public String getFlag() {
        return Flag;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isLocalPhrase(){
        if( this.location !=null){
            return !this.location.equals("na");
        }else{
            return false;
        }
    }

}
