package ca.dal.atozvocab.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ca.dal.atozvocab.fragments.FlashcardsFragment;
import ca.dal.atozvocab.fragments.MarkedWordsFragment;
import ca.dal.atozvocab.R;
import ca.dal.atozvocab.models.UserDataModel;
import ca.dal.atozvocab.fragments.WordsFragment;
import ca.dal.atozvocab.models.WordsModel;

public class DashboardActivity extends AppCompatActivity implements FlashcardsFragment.OnFragmentInteractionListener {

    private String mCurUserEmail="";
    private final String defaultLearningFocus = "general";//"gre";
    private String mCurLearningFocus = defaultLearningFocus;
    private String mCurLanugagePref = "US";


    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private FirebaseFirestore mFirestore;
    private UserDataModel mUserData = new UserDataModel();
    final String TAG = "DashboardActivity";

    LocationManager locationManager;
    LocationListener locationListener;

    TextView mLocation;
    Button mPickLocation, mDone;
    int placePickerCode = 1;

    Spinner mSpinner;
    Button mLanguageDone;

    TextView mUserEmail;
    TextView mStatusBar;

    boolean uncheckRequired = true;

    public static String accentPreference = "US";

    WordsFragment mWordsFragInstance = null;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fragManager = getSupportFragmentManager();
            FragmentTransaction fragTx = fragManager.beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_marked_words:
                    getSupportActionBar().setTitle("Marked Words");
                    fragTx.replace(R.id.content,
                            new MarkedWordsFragment().newInstance(mCurUserEmail,mCurLanugagePref)).commit();
                    setFragmentWelcomeMsg();
                    return true;
                case R.id.navigation_word_list:
                    getSupportActionBar().setTitle("Words List");
                    if(mWordsFragInstance == null){
                        mWordsFragInstance = new WordsFragment().newInstance(
                                mCurUserEmail, mCurLearningFocus,mCurLanugagePref, mCurLanugagePref);
                    }
                    fragTx.replace(R.id.content,mWordsFragInstance).commit();
                    return true;
                case R.id.navigation_flash_cards:
                    getSupportActionBar().setTitle("Flash Cards");
                    if(getIntent().getStringExtra("email").equals("")) {
                        Toast.makeText(DashboardActivity.this,
                                "Please Register to use the flash card feature",
                                Toast.LENGTH_SHORT).show();
                    }
                    else {
                        fragTx.replace(R.id.content,
                                FlashcardsFragment.newInstance(0,
                                        getIntent().getStringExtra("email"))).commit();
                        return true;
                    }
            }
            return false;
        }
    };

    /**
     * This class fires on click on known word button in flash card UI
     *
     * @param view     Flashcard Fragment view Object
     * @param wordFreq frequency of known word displayed
     * @param wordsModel word model
     */
    @Override
    public void onKnownWordClick(View view, WordsModel wordsModel, int wordFreq) {
        FlashcardsFragment.newInstance(wordFreq, getIntent().getStringExtra("email")).onNextClick(view, wordsModel);
    }

    /**
     * This class fires on click on Unknown word button in flash card UI
     *
     * @param flag Unknown word
     * @param wordsModel word model
     * @param view  Flashcard Fragment view Object
     * @param  wordFreq known word Frequency
     */
    @Override
    public void onUnKnownWordClick(View view, WordsModel wordsModel, String flag, int wordFreq) {
        FlashcardsFragment.newInstance(wordFreq, getIntent().getStringExtra("email")).onDontKnowClick(view, wordsModel, flag);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.INTERNET}
                        ,10);
            }
        }

        mFirestore = FirebaseFirestore.getInstance();
        mCurUserEmail = getIntent().getExtras().getString("email");

        BottomNavigationView navBottom = (BottomNavigationView) findViewById(R.id.navigation);
        navBottom.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if(mCurUserEmail.equals("")){
            /* On page launch, show words-list fragment.*/
            navBottom.setSelectedItemId(R.id.navigation_word_list);
        }else{
            /* Get user data model*/
            DocumentReference refUserDoc = mFirestore.collection("UserDataModel").document(mCurUserEmail);
            refUserDoc.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            mUserData = document.toObject(UserDataModel.class);
                            Log.d(TAG, "onCreateView DocumentSnapshot data: " + document.getData());
                        } else {
                            Log.d(TAG, "Document doesn't exist! " );
                        /* Create UserDataModel */
                            UserDataModel udm = new UserDataModel();
                            udm.setMarkedWords(new ArrayList<String>());
                            udm.setKnownWords(new ArrayList<String>());
                            udm.setEmail(mCurUserEmail);
                            udm.setName(mCurUserEmail); //TODO: Change it
                            udm.setLearningFocus(defaultLearningFocus);
                            mFirestore.collection("UserDataModel").document(mCurUserEmail).set(udm)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.d(TAG, "UserDataModel successfully written!");
                                            mUserData = udm;
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.w(TAG, "Error writing document", e);
                                        }
                                    });
                        }
                        /* On page launch, show words-list fragment.*/
                        navBottom.setSelectedItemId(R.id.navigation_word_list);
                    } else {
                        Log.d(TAG, "get failed with ", task.getException());
                    }
                }
            });
        }
        View mHeaderView = getLayoutInflater().inflate(R.layout.header, null);
        mUserEmail = (TextView) mHeaderView.findViewById(R.id.userEmail);
        mUserEmail.setText(mCurUserEmail);          //Set email id of user in header.xml

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.bringToFront();
        navigationView.addHeaderView(mHeaderView);

        ImageView mImageView = (ImageView) MenuItemCompat.getActionView(navigationView.getMenu().
                findItem(R.id.nav_local_phrases));
        mImageView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mImageView.setBackground(getResources().getDrawable(R.drawable.ic_gps_fixed_black_24dp));
        mImageView.setScaleY(0.5f);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if(uncheckRequired == true) {
                    uncheckRequired = false;
                    uncheckItems(navigationView);
                }

                FragmentManager fragManager = getSupportFragmentManager();
                FragmentTransaction fragTx = fragManager.beginTransaction();
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);

                int id = item.getItemId();
                if (id == R.id.nav_gre) {
                    mCurLearningFocus = "gre";
                    mWordsFragInstance = new WordsFragment().newInstance(mCurUserEmail,mCurLearningFocus,"", mCurLanugagePref);
                    fragTx.replace(R.id.content, mWordsFragInstance).commit();
                    drawer.closeDrawer(GravityCompat.START);
                    return true;
                } else if (id == R.id.nav_sat) {
                    mCurLearningFocus = "sat";
                    mWordsFragInstance = new WordsFragment().newInstance(mCurUserEmail,mCurLearningFocus,"", mCurLanugagePref);
                    fragTx.replace(R.id.content, mWordsFragInstance).commit();
                    drawer.closeDrawer(GravityCompat.START);
                    return true;
                } else if (id == R.id.nav_general) {
                    mCurLearningFocus = "general";
                    mWordsFragInstance = new WordsFragment().newInstance(mCurUserEmail,mCurLearningFocus,"", mCurLanugagePref);
                    fragTx.replace(R.id.content, mWordsFragInstance).commit();
                    drawer.closeDrawer(GravityCompat.START);
                    return true;
                } else if (id == R.id.nav_local_phrases) {
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(DashboardActivity.this);
                    View mView = getLayoutInflater().inflate(R.layout.custom_location_dialog, null);
                    mLocation = (TextView) mView.findViewById(R.id.location);
                    mPickLocation = (Button) mView.findViewById(R.id.pickLocation);
                    mDone = (Button) mView.findViewById(R.id.doneButton);

                    drawer.closeDrawer(GravityCompat.START);
                    mBuilder.setView(mView);
                    AlertDialog dialog = mBuilder.create();
                    if (ActivityCompat.checkSelfPermission(DashboardActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(DashboardActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION,
                                            Manifest.permission.INTERNET}
                                    ,10);
                        }
                    }
                    getCurrentLocation();
                    mPickLocation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getPlacePicker();
                        }
                    });

                    mDone.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String countrySelected = mLocation.getText().toString();
                            if(countrySelected.equalsIgnoreCase("United States"))
                                countrySelected = "USA";
                            countrySelected = countrySelected.replaceAll("\\s+","");
                            if(countrySelected.equalsIgnoreCase("India") || countrySelected.equalsIgnoreCase("Canada") || countrySelected.equalsIgnoreCase("USA"))
                            {
                                dialog.dismiss();
                                mCurLearningFocus = "local_phrases";
                                fragTx.replace(R.id.content,
                                        new WordsFragment().newInstance(mCurUserEmail,mCurLearningFocus,countrySelected, mCurLanugagePref)).commit();
                                drawer.closeDrawer(GravityCompat.START);
                            }
                            else
                            {
                                Toast.makeText(DashboardActivity.this, "Only India, Canada & USA are supported", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    dialog.show();
                    return true;
                } else if (id == R.id.nav_accent) {
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(DashboardActivity.this);
                    View mView = getLayoutInflater().inflate(R.layout.custom_language_dialog    , null);
                    mSpinner = (Spinner) mView.findViewById(R.id.spinner);
                    mLanguageDone = (Button) mView.findViewById(R.id.languageButton);

                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(DashboardActivity.this, android.R.layout.simple_list_item_1,
                            getResources().getStringArray(R.array.locations));

                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    mSpinner.setAdapter(arrayAdapter);

                    if(!mCurLanugagePref.equalsIgnoreCase(""))
                    {
                        int position = arrayAdapter.getPosition(mCurLanugagePref);
                        mSpinner.setSelection(position);
                    }

                    mBuilder.setView(mView);
                    AlertDialog dialog = mBuilder.create();
                    dialog.show();
                    mLanguageDone.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            boolean flag =true;
                            String text = mSpinner.getSelectedItem().toString();
                                mCurLanugagePref = "" +text;
                                accentPreference = "" + text;
                                dialog.dismiss();
                        }
                    });
                }
                else if (id == R.id.nav_logout) {
                    FirebaseAuth.getInstance().signOut();
                    finish();
                }
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });
    }

    private void setFragmentWelcomeMsg(){
        String text = "Welcome "+mCurUserEmail;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*Get Current Location*/
    public void getCurrentLocation() {
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED )
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        else
            Toast.makeText(DashboardActivity.this, "Location Permission Missing",Toast.LENGTH_SHORT).show();
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                String countryName = getCountryName(DashboardActivity.this,location.getLatitude(), location.getLongitude());
                if(mLocation.getText().toString().equalsIgnoreCase("pick a location"))
                    mLocation.setText("" +countryName);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            }
        };

        configureButton();
    }
    /*Configuring Location*/
    public void configureButton() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.INTERNET}
                        ,10);
            }
        }
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED )
                locationManager.requestLocationUpdates("gps", 1, 0, locationListener);
                Location location = null;
                if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED )
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if(location!=null)
                {
                    String countryName = getCountryName(DashboardActivity.this,location.getLatitude(), location.getLongitude());
                    mLocation.setText("" +countryName);
                }
                else
                {
                    mLocation.setText("Pick a location");
                }
    }

    /*On Request for Permissions*/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case 10:
                if( grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    return;
                }
                break;
            default:
                break;
        }
    }

    /*
    Reqturns the Country name
     */
    public static String getCountryName(Context context, double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            Address result;

            if (addresses != null && !addresses.isEmpty()) {
                return addresses.get(0).getCountryName();
            }
            return null;
        } catch (IOException ignored) {
            ignored.printStackTrace();
        }
        return "Empty";
    }

    /*
    Calls the Place Picker API
     */
    public void getPlacePicker()
    {
        PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
        Intent placeIntent;
        try {
            placeIntent = intentBuilder.build(this);
            startActivityForResult(placeIntent, placePickerCode);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    /*
    Called, when a location is selected from Place Picker
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        if(requestCode == placePickerCode)
        {
            if(resultCode == RESULT_OK)
            {
                Place place = PlacePicker.getPlace(intent, this);
                String address = "" +place.getAddress();
                String blocks[] = address.split(",");
                Log.i("Address: ", "we have found " +address);
                mLocation.setText(blocks[blocks.length-1]);
            }
        }
    }

    /*
    Uncheck all the items in learning focus
     */
    private void uncheckItems(NavigationView navigationView) {
        Menu mainMenu = navigationView.getMenu();
        for(int i=0; i<mainMenu.size();i++)
        {
            MenuItem menuItem = mainMenu.getItem(i);
            if (menuItem.hasSubMenu())
            {
                SubMenu subMenu = menuItem.getSubMenu();
                for (int j = 0; j < subMenu.size(); j++)
                {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    subMenuItem.setChecked(false);
                }
            }
            else
                menuItem.setChecked(false);
        }
    }
}

