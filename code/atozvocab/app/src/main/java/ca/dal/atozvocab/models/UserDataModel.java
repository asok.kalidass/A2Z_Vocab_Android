package ca.dal.atozvocab.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kundan on 2017-11-15.
 */

public class UserDataModel {
    private String name;
    private String email;
    private String learningFocus;
    private List<String> markedWords;
    private List<String> knownWords;

    public UserDataModel(String name, String email, List<String> markedWords, String learningFocus,
                         List<String> knownWords) {
        this.name = name;
        this.email = email;
        this.markedWords = markedWords;
        this.learningFocus = learningFocus;
        this.knownWords = knownWords;
    }
    public UserDataModel() {}

    public UserDataModel(String name, String email, String learningFocus, String[] markedWords, String[] knownWords) {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLearningFocus() {
        return learningFocus;
    }

    public void setLearningFocus(String learningFocus) {
        this.learningFocus = learningFocus;
    }

    public List<String> getMarkedWords() {
        return markedWords;
    }

    public void setMarkedWords(List<String> markedWords) {
        this.markedWords = markedWords;
    }

    public List<String> getKnownWords() {
        return knownWords;
    }

    public void setKnownWords(List<String> knownWords) {this.knownWords = knownWords;}
}
