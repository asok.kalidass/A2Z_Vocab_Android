package ca.dal.atozvocab.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import ca.dal.atozvocab.R;
import ca.dal.atozvocab.utils.SpeechSynthesizer;
import ca.dal.atozvocab.models.UserDataModel;
import ca.dal.atozvocab.utils.WordsAdapter;
import ca.dal.atozvocab.models.WordsModel;
import ca.dal.atozvocab.activities.DashboardActivity;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WordsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WordsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WordsFragment extends Fragment implements WordsAdapter.WordsAdapterCallback {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";

    private String mCurUserEmail="";
    private String mCurLearningFocus;
    private String mPhraseLoc="";
    private String mCurLanguagePref = "";

    private OnFragmentInteractionListener mListener;
    private FirebaseFirestore mFirestore;
    RecyclerView mRv;
    View rootView;
    boolean mIsGreSelected = false;
    boolean mIsSatSelected = false;
    boolean mIsGeneralSelected = true;

    Context mContext;
    private int mTop = -1;
    private int mIndex = -1;
    private WordsAdapter mWordsAdapter;
    LinearLayoutManager mLayoutMgr;

    public WordsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param email Parameter 1.
     * @param learningFocus Parameter 2.
     * @return A new instance of fragment WordsFragment.
     */
    public static WordsFragment newInstance(String email, String learningFocus, String phraseLoc, String languagePref) {
        WordsFragment fragment = new WordsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, email);
        args.putString(ARG_PARAM2, learningFocus);
        args.putString(ARG_PARAM3, phraseLoc);
        args.putString(ARG_PARAM4, languagePref);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCurUserEmail = getArguments().getString(ARG_PARAM1);
            mCurLearningFocus = getArguments().getString(ARG_PARAM2);
            mPhraseLoc = getArguments().getString(ARG_PARAM3);
            mCurLanguagePref = getArguments().getString(ARG_PARAM4);
        }
    }

    private List<WordsModel> mWModels = null;
    private UserDataModel mUserData = new UserDataModel();
    List<WordsModel> mDisplayedWordsModel = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mIsGreSelected = mCurLearningFocus.equals("gre");
        mIsSatSelected = mCurLearningFocus.equals("sat");
        mIsGeneralSelected = mCurLearningFocus.equals("general");

        HashMap<String, String> learningFocus = new HashMap<String, String>();
        learningFocus.put("gre", "GRE Words");
        learningFocus.put("sat", "SAT Words");
        learningFocus.put("general", "General Words");
        learningFocus.put("local_phrases", "Local Phrases");

        View rootView = inflater.inflate(R.layout.fragment_words, container, false);
        mRv = (RecyclerView) rootView.findViewById(R.id.words_rv);
        mRv.setHasFixedSize(true);

        TextView mStatusBar = (TextView) rootView.findViewById(R.id.statusBar);
        if(!mCurLearningFocus.equalsIgnoreCase("local_phrases"))
        {
            mStatusBar.setText(learningFocus.get(mCurLearningFocus));
        }
        else
        {
            mStatusBar.setText(learningFocus.get(mCurLearningFocus) +" (" +mPhraseLoc +")");
        }




        CollectionReference refWordsModel = mFirestore.collection("WordsModel");
        refWordsModel.addSnapshotListener((snapshot, e) -> {
            if (e != null) {
                Log.e("onCreateView", "refWordsModel Listen failed.", e);
                return;
            }
            mWModels  = new ArrayList<WordsModel>();
            for (DocumentSnapshot document : snapshot.getDocuments()) {
                WordsModel wm = document.toObject(WordsModel.class);
                mWModels.add(wm);
            }
            updateView();
        });

        if(!mCurUserEmail.equals("")){
            /* addSnapshotListener is not used to reflect remove item animation.*/
            DocumentReference refUserDoc = mFirestore.collection("UserDataModel").document(mCurUserEmail);
            refUserDoc.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document != null) {
                            Log.d("onCreateView", "DocumentSnapshot data: " + task.getResult().getData());
                            mUserData = document.toObject(UserDataModel.class);
                            updateView();
                        } else {
                            Log.d("onCreateView", "No such document");
                        }
                    } else {
                        Log.d("onCreateView", "get failed with ", task.getException());
                    }
                }
            });
        }
        // Inflate the layout for this fragment
        return rootView;
    }

    public void updateView(){
        if ((mWModels ==null)){
            return;
        }
        mDisplayedWordsModel =  new ArrayList<WordsModel>();
        for(int idx=0; idx<mWModels.size();idx++){
            WordsModel wm = mWModels.get(idx);
            boolean isMarkedByUser = (mUserData.getMarkedWords() != null) ?
                    (mUserData.getMarkedWords().contains(wm.getWord())): false;
            boolean isValidLearningFocus =
                    (wm.isGre() && mIsGreSelected) ||
                    (wm.isSat() && mIsSatSelected) ||
                    (wm.isGeneral() && mIsGeneralSelected) ||
                    (wm.isLocalPhrase() &&  mPhraseLoc.equalsIgnoreCase(wm.getLocation()));

            if( isValidLearningFocus && !isMarkedByUser ){
                mDisplayedWordsModel.add(wm);
            }
        }

        WordsAdapter.E_VIEW_TYPE viewType;
        if(mCurUserEmail.equals("")){
            viewType = WordsAdapter.E_VIEW_TYPE.NONE;
        }else{
            viewType = WordsAdapter.E_VIEW_TYPE.WORDS_LIST;
        }
        mWordsAdapter = new WordsAdapter(mDisplayedWordsModel, viewType, this);
        mRv.setAdapter(mWordsAdapter);
        mLayoutMgr = new LinearLayoutManager(getActivity());
        mRv.setLayoutManager(mLayoutMgr);
        if(mIndex != -1) {
            mLayoutMgr.scrollToPositionWithOffset( mIndex, mTop);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            mContext = context;
            mFirestore = FirebaseFirestore.getInstance();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onMarkWordsCallback(String wordRemoved) {
        if(mCurUserEmail.equals("")){
            Toast.makeText(mContext,"Please login to use this feature.", Toast.LENGTH_SHORT).show();
            return;
        }

        /* Update user's markedWords list in the database */
        List<String> markedWords = mUserData.getMarkedWords();
        if(markedWords == null){
            markedWords = new ArrayList<String>();
        }
        markedWords.add(wordRemoved);
        mUserData.setMarkedWords(markedWords);
        mFirestore.collection("UserDataModel").document(mCurUserEmail).set(mUserData)
        .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d("addOnSuccessListener","addOnSuccessListener");
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            Log.e("onMarkWordsCallback","addOnFailureListener: "+e.toString());
            }
        });
    }

    /**
     * Read Aloud
     * @param word  word to speak aloud
     */
    @Override
    public void onReadAloudCallback(String word) {
        Log.v("onReadAloudCallback", "Read Aloud the Words at pos");
        new SpeechSynthesizer(getContext(), getUserLocation(), word);
    }

    /**
     * Get the Locale based on user selected location
     * @return location User chosen selection
     */
    private Locale getUserLocation() {
        Locale location;
        switch(DashboardActivity.accentPreference) {
            case "CANADA":
                location = Locale.CANADA;
                break;
            case "ITALY":
                location = Locale.ITALY;
                break;
            case "FRANCE":
                location = Locale.FRANCE;
                break;
            case "GERMAN":
                location = Locale.GERMAN;
                break;
            case "UK":
                location = Locale.UK;
                break;
            default:
                location = Locale.US;
                break;
        }
        return  location;
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onPause() {
        super.onPause();
        mIndex = mLayoutMgr.findFirstVisibleItemPosition();
        View v = mRv.getChildAt(0);
        mTop = (v == null) ? 0 : (v.getTop() - mRv.getPaddingTop());
    }
}
