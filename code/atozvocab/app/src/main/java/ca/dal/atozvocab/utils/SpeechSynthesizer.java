package ca.dal.atozvocab.utils;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import java.util.Locale;

/**
 * Created by Asok on 2017-11-13.
 */

public class SpeechSynthesizer implements TextToSpeech.OnInitListener{
    //Declarations
    TextToSpeech textToSpeech;
    Locale location;
    String[] words;
    // TextToSpeech instance.
    public SpeechSynthesizer(Context context, Locale location, String words) {
        textToSpeech = new TextToSpeech(context, this);
        this.location = location;
        this.words = words.split(",");
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int languageCode = textToSpeech.setLanguage(location);

            if (languageCode == TextToSpeech.LANG_MISSING_DATA
                    || languageCode == TextToSpeech.LANG_NOT_SUPPORTED) {
                //language is not supported
                Log.w("onInit", "Language not supported.");
            } else {
                speakLoud();
            }
        }
        textToSpeech.shutdown();
    }

    //Speak aloud the text
    private void speakLoud() {
        for (String word: words) {
            textToSpeech.speak(word, TextToSpeech.QUEUE_ADD, null, null);
            while (textToSpeech.isSpeaking() ) {
            };
        }
    }

}
