package ca.dal.atozvocab.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ca.dal.atozvocab.R;
import ca.dal.atozvocab.utils.SpeechSynthesizer;
import ca.dal.atozvocab.models.UserDataModel;
import ca.dal.atozvocab.utils.WordsAdapter;
import ca.dal.atozvocab.models.WordsModel;
import ca.dal.atozvocab.activities.DashboardActivity;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MarkedWordsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MarkedWordsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MarkedWordsFragment extends Fragment implements WordsAdapter.WordsAdapterCallback {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mCurUserEmail="";
    private String mCurLanguagePref="";

    RecyclerView mRv;
    private WordsAdapter mWordsAdapter;
    private List<WordsModel> mWordsModel = null;
    List<WordsModel> mDisplayedWordsModel = null;

    private FirebaseFirestore mFirestore;
    DocumentReference mRefUserDoc;
    CollectionReference mRefWordsModel;
    private UserDataModel mUserData = new UserDataModel();

    private OnFragmentInteractionListener mListener;
    Context mContext;

    public MarkedWordsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param email Parameter 1.
     * @param languagePref Parameter 2.
     * @return A new instance of fragment MarkedWordsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MarkedWordsFragment newInstance(String email, String languagePref) {
        MarkedWordsFragment fragment = new MarkedWordsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, email);
        args.putString(ARG_PARAM2, languagePref);
        Log.i(TAG,"Email is: :"+email);
        Log.i(TAG,"Language Preference is:"+languagePref);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCurUserEmail = getArguments().getString(ARG_PARAM1);
            mCurLanguagePref = getArguments().getString(ARG_PARAM2);
            mRefWordsModel = mFirestore.collection("WordsModel");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_marked_words, container, false);
        mRv = (RecyclerView) rootView.findViewById(R.id.marked_words_rv);
        mRv.setHasFixedSize(true);

        /* Get words */
        mRefWordsModel.addSnapshotListener((snapshot, e) -> {
            if (e != null) {
                Log.e("onCreateView", "refWordsModel Listen failed.", e);
                return;
            }
            mWordsModel  = new ArrayList<WordsModel>();
            for (DocumentSnapshot document : snapshot.getDocuments()) {
                WordsModel wm = document.toObject(WordsModel.class);
                mWordsModel.add(wm);
            }
            updateView();
        });

        /* Get Marked-words */
        if(!mCurUserEmail.equals("")){
            mRefUserDoc = mFirestore.collection("UserDataModel").document(mCurUserEmail);
            mRefUserDoc.addSnapshotListener((snapshot, e) -> {
                if (e != null) {
                    Log.e("onCreateView", "refUserDoc Listen failed!", e);
                    return;
                }
                if(snapshot.exists()){
                    mUserData = snapshot.toObject(UserDataModel.class);
                    Log.i("mRefUserDoc", "Received data...."+snapshot.getData());
                    updateView();
                }
            });
        }

        return rootView;
    }

    public void updateView(){
        if( mUserData.getMarkedWords() != null){
            mDisplayedWordsModel =  new ArrayList<WordsModel>();
            int markedWordsCnt = mUserData.getMarkedWords().size();
            List<String> userMarkedWords = mUserData.getMarkedWords();

            int totalWords= mWordsModel.size();
            for(int m=0; m<markedWordsCnt; m++){
                String mw = userMarkedWords.get(m);
                for(int n=0; n<totalWords; n++){
                    if (mWordsModel.get(n).getWord().equals(mw)){
                        mDisplayedWordsModel.add(mWordsModel.get(n));
                        Log.i("Added Word", mWordsModel.get(n).getWord());
                    }
                }
            }
        }

        if (mDisplayedWordsModel!=null){
            Log.e("After add", "Size is "+mDisplayedWordsModel.size());
            mWordsAdapter = new WordsAdapter(mDisplayedWordsModel,WordsAdapter.E_VIEW_TYPE.MARKED_WORDS, this);
            mRv.setAdapter(mWordsAdapter);
            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            mRv.setLayoutManager(llm);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            mContext = context;
            mFirestore = FirebaseFirestore.getInstance();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override //Note: It should be named as onMarkUnmarkWordsCallback
    public void onMarkWordsCallback(String wordRemoved) {

        if(mCurUserEmail.equals("")){
            Toast.makeText(mContext,"Please login to use this feature.", Toast.LENGTH_SHORT).show();
            return;
        }

        /* Update user's markedWords list in the database */
        List<String> markedWords = mUserData.getMarkedWords();
        if(markedWords == null){
            markedWords = new ArrayList<String>();
        }
        markedWords.remove(wordRemoved);
        mUserData.setMarkedWords(markedWords);
        mFirestore.collection("UserDataModel").document(mCurUserEmail).set(mUserData)
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.d("addOnSuccessListener","addOnSuccessListener");
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("onMarkWordsCallback","addOnFailureListener: "+e.toString());
                }
            });
    }

    /**
     * Read Aloud
     * @param word word to speak aloud
     */
    @Override
    public void onReadAloudCallback(String word) {
        Log.e("onReadAloudCallback", "Read Aloud the Words");
        new SpeechSynthesizer(getContext(), getUserLocation(), word);
    }

    /**
     * Get the Locale based on user selected location
     * @return location User chosen selection
     */
    private Locale getUserLocation() {
        Locale location;
        switch(DashboardActivity.accentPreference) {
            case "CANADA":
                location = Locale.CANADA;
                break;
            case "ITALY":
                location = Locale.ITALY;
                break;
            case "FRANCE":
                location = Locale.FRANCE;
                break;
            case "GERMAN":
                location = Locale.GERMAN;
                break;
            case "UK":
                location = Locale.UK;
                break;
            default:
                location = Locale.US;
                break;
        }
        return  location;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
