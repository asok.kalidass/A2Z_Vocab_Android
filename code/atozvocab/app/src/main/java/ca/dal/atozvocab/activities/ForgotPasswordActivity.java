package ca.dal.atozvocab.activities;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import ca.dal.atozvocab.R;

public class ForgotPasswordActivity extends AppCompatActivity {

    FirebaseAuth mAuth;
    private final String TAG = "ForgotPasswordActivity";
    private final String mSuccessMsg = "Password reset mail sent";
    private final String mFailureMessage = "Failed to send password reset mail. "+
            "Please check if the entered email is correct and you have an active internet connection.";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        mAuth = FirebaseAuth.getInstance();
    }

    public void resetUserPassword(View view) {
        EditText emailTxt = (EditText)findViewById(R.id.etEmailFp);
        String email = emailTxt.getText().toString();
        if(email.isEmpty()){
            Toast.makeText(getApplicationContext(),
                    "Please enter a valid email address.", Toast.LENGTH_SHORT).show();
            return;
        }
        mAuth.sendPasswordResetEmail(email)
            .addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Toast.makeText(getApplicationContext(),
                                mSuccessMsg+" to "+email, Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplicationContext(),
                                mFailureMessage, Toast.LENGTH_SHORT).show();
                    }
                }
            });
    }
}
