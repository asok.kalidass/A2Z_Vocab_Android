package ca.dal.atozvocab.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import ca.dal.atozvocab.R;

public class RegistrationActivity extends AppCompatActivity {

    private EditText mEtEmailRegister;
    private EditText mEtPwdLogin;
    private FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        mEtEmailRegister = (EditText) findViewById(R.id.etEmailRegister);
        mEtPwdLogin = (EditText) findViewById(R.id.etPwdLogin);
        FirebaseApp.initializeApp(this);
        firebaseAuth = FirebaseAuth.getInstance();
        setTitle("Register");
    }
    public void btnRegisterUser(View v) {

        final ProgressDialog progressDialog = ProgressDialog.show(RegistrationActivity.this, "Please wait...", "Processing...", true);
        String email = mEtEmailRegister.getText().toString();
        String pwd = mEtPwdLogin.getText().toString();
        (firebaseAuth.createUserWithEmailAndPassword(email, pwd))
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();
                        if (task.isSuccessful()) {
                            Toast.makeText(RegistrationActivity.this, "Registration successful", Toast.LENGTH_LONG).show();
                            Intent i = new Intent(RegistrationActivity.this, MainActivity.class);
                            startActivity(i);
                        }
                        else
                        {
                            Log.e("RegistrationActivity", task.getException().toString());
                            Toast.makeText(RegistrationActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}
